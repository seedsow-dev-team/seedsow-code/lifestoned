﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

using log4net;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle.Spells;
using Lifestoned.DataModel.Shared;
using Lifestoned.Providers;

namespace DerethForever.Web.Controllers
{
    public class SpellTableController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ISpellTableProvider Provider => ContentProviderHost.GetProvider<ISpellTableProvider>(SpellTableChange.TypeName);

        private ISpellTableSandboxProvider SandboxProvider => SandboxContentProviderHost.GetProvider<ISpellTableSandboxProvider>(SpellTableChange.TypeName);

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Get(uint id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Guid userId = new Guid(GetUserGuid());
                ChangeEntry<SpellTableEntry> change = SandboxProvider.GetChange(userId, id);
                if (change != null)
                    return JsonGet(change.Entry, true);
            }

            SpellTableEntry model = Provider.Get(id);
            return JsonGet(model, true);
        }

        [HttpGet]
        public ActionResult Search(string name, uint? school, uint? category)
        {
            IQueryable<SpellTableEntry> model = null;
            if (User.Identity.IsAuthenticated)
            {
                model = SandboxProvider.Search(new Guid(GetUserGuid()), (s) => s.Value.Name == name && (school != null ? s.Value.School == school : true) && (category != null ? s.Value.Category == category : true));
            }
            else
            {
                model = Provider.Search((s) => s.Value.Name == name && (school != null ? s.Value.School == school : true) && (category != null ? s.Value.Category == category : true));
            }

            return JsonGet(model, true);
        }

        [HttpPut]
        ////[Authorize(Roles = "Admin")]
        [Authorize]
        public ActionResult Put([ModelBinder(typeof(JsonNetModelBinder))]SpellTableEntry model)
        {
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            Guid userId = new Guid(GetUserGuid());

            try
            {
                SandboxProvider.UpdateChange(userId, model);
                ////ContentProviderHost.CurrentSpellTableProvider.Save(model);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Error($"Error Saving Spell {model.Key} - {model.Value.Description}", ex);
            }

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError, "Save Failed");
        }

        [HttpGet]
        public ActionResult Edit(uint? id)
        {
            return View(id);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(uint id)
        {
            return View(Provider.Get(id));
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(SpellTableEntry model)
        {
            Provider.Delete(model.Key);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult UploadItem()
        {
            string fileNameCopy = "n/a";

            try
            {
                foreach (string fileName in Request.Files)
                {
                    fileNameCopy = fileName;
                    HttpPostedFileBase file = Request.Files[fileName];

                    using (MemoryStream memStream = new MemoryStream())
                    {
                        file.InputStream.CopyTo(memStream);
                        byte[] data = memStream.ToArray();

                        string token = GetUserToken();
                        string userName = GetUserName();

                        string serialized = Encoding.UTF8.GetString(data);
                        SpellsFile table = JsonConvert.DeserializeObject<SpellsFile>(serialized);

                        foreach (SpellTableEntry entry in table.Table.Entries)
                        {
                            entry.LastModified = DateTime.Now;
                            entry.ModifiedBy = userName;

                            // save it
                            ////ContentProviderHost.CurrentSpellTableProvider.Save(entry);
                            Provider.Save(entry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error parsing uploaded file {fileNameCopy}.", ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult DownloadOriginal(uint id)
        {
            try
            {
                SpellTableEntry model = Provider.Get(id);
                return DownloadJson(model, model.Key, model.Value.Description);
            }
            catch (Exception ex)
            {
                log.Error($"Error exporting spell {id}", ex);
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult DownloadSandbox(uint id, string userGuid)
        {
            Guid userId = Guid.Empty;

            if (!string.IsNullOrWhiteSpace(userGuid) && (userGuid == GetUserGuid() || User.IsInRole("Developer")))
            {
                userId = new Guid(userGuid);
            }
            else
            {
                userId = new Guid(GetUserGuid());
            }

            try
            {
                ChangeEntry<SpellTableEntry> change = SandboxProvider.GetChange(userId, id);
                if (change == null)
                {
                    return HttpNotFound();
                }

                return DownloadJson(change.Entry, change.EntryId, change.Entry.Value.Name);
            }
            catch (Exception ex)
            {
                log.Error($"Error exporting spell {id}", ex);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }
    }
}