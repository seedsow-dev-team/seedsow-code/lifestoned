﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lifestoned.DataModel.Account;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Gdle.Spells;
using Lifestoned.DataModel.Shared;
using Lifestoned.Providers;
using Newtonsoft.Json;

namespace DerethForever.Web.Controllers
{
    [Authorize]
    public class SandboxController : BaseController
    {
        // GET: Sandbox
        public ActionResult Index()
        {
            SandboxModel model = new SandboxModel();
            Guid uid = new Guid(GetUserGuid());
            model.Entries = SandboxContentProviderHost.GetChangeEntries(uid).ToList();
            ////model.Entries = SandboxContentProviderHost.CurrentProvider.GetChanges(new Guid(GetUserGuid())).ToList();
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Delete(uint id, string type)
        {
            Guid uid = new Guid(GetUserGuid());
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                SandboxContentProviderHost.DeleteChange(uid, entry);
                ////SandboxContentProviderHost.CurrentProvider.DeleteChange(uid, entry);
            }

            return Json(true);
        }

        [HttpGet]
        [Authorize(Roles = "Developer")]
        public ActionResult Submissions()
        {
            SandboxModel model = new SandboxModel();
            BaseModel current = CurrentBaseModel;
            BaseModel.CopyBaseData(current, model);
            CurrentBaseModel = current;

            IEnumerable<ChangeEntry> entries = null;

            if (User.IsInRole("Developer"))
                entries = SandboxContentProviderHost.GetChangeEntries();
            ////entries = SandboxContentProviderHost.CurrentProvider.GetChanges();
            else
                entries = SandboxContentProviderHost.GetChangeEntries(new Guid(GetUserGuid()));
            ////entries = SandboxContentProviderHost.CurrentProvider.GetChanges(new Guid(GetUserGuid()));

            model.Entries = entries.Where(x => x.Submitted).ToList();

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddDiscussionComment(string userGuid, uint itemId, string type, string discussionComment, string source)
        {
            string currentUser = GetUserGuid();

            if (!(User.IsInRole("Developer") || userGuid == currentUser))
            {
                // only the submitter and developers can comment
                return RedirectToAction(source);
            }

            Guid uid = new Guid(userGuid);

            ChangeEntry change = SandboxContentProviderHost.GetChangeEntry(type, uid, itemId);
            ////IEnumerable<ChangeEntry> temp = SandboxContentProviderHost.CurrentProvider.GetChanges(new Guid(userGuid), type);
            ////ChangeEntry change = temp.FirstOrDefault(x => x.UserGuid == userGuid && x.EntryId == itemId);

            if (change == null)
                return RedirectToAction(source);

            change.Discussion.Add(new ChangeDiscussionEntry()
            {
                Comment = discussionComment,
                Created = DateTime.Now,
                UserName = GetUserName(),
                UserGuid = Guid.Parse(GetUserGuid())
            });

            ////SandboxContentProviderHost.CurrentProvider.UpdateChange(new Guid(userGuid), change);
            SandboxContentProviderHost.UpdateChange(uid, change);

            return RedirectToAction(source);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Submit(uint id, string type)
        {
            Guid uid = new Guid(GetUserGuid());
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                entry.Submitted = true;
                entry.SubmissionTime = DateTime.Now;
                ////SandboxContentProviderHost.CurrentProvider.UpdateChange(uid, entry);
                SandboxContentProviderHost.UpdateChange(uid, entry);

                DiscordController.PostChangeAsync(entry);
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Withdraw(uint id, string type)
        {
            Guid uid = new Guid(GetUserGuid());
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                entry.Submitted = false;
                ////SandboxContentProviderHost.CurrentProvider.UpdateChange(uid, entry);
                SandboxContentProviderHost.UpdateChange(uid, entry);
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize(Roles = "Developer")]
        public ActionResult Accept(uint id, string type, string userId, bool done)
        {
            Guid uid = new Guid(userId);
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                if (!string.IsNullOrEmpty(entry.ChangeLog.UserChangeSummary))
                {
                    entry.ChangeLog.Changelog.Add(new ChangelogEntry()
                    {
                        Author = entry.UserName,
                        Created = entry.SubmissionTime,
                        Comment = entry.ChangeLog.UserChangeSummary
                    });
                }

                if (done)
                    entry.Metadata.IsDone = true;

                // copy to final
                ////SandboxContentProviderHost.CurrentProvider.AcceptChange(uid, entry);
                SandboxContentProviderHost.AcceptChange(uid, entry);

                // delete change
                ////SandboxContentProviderHost.CurrentProvider.DeleteChange(uid, entry);
                SandboxContentProviderHost.DeleteChange(uid, entry);

                // submit to discord
                DiscordController.PostAcceptAsync(entry, CurrentUser.DisplayName);
            }

            return new EmptyResult();
        }

        [HttpPost]
        [Authorize(Roles = "Developer")]
        public ActionResult Reject(uint id, string type, string userId, string comment)
        {
            Guid uid = new Guid(userId);
            ChangeEntry entry = SandboxContentProviderHost.GetChangeEntry(type, uid, id);
            ////ChangeEntry entry = SandboxContentProviderHost.CurrentProvider.GetChanges(uid, type)
            ////    .FirstOrDefault(c => c.EntryId == id);

            if (entry != null)
            {
                entry.Submitted = false;
                entry.Discussion.Add(new ChangeDiscussionEntry()
                {
                    Comment = comment,
                    Created = DateTime.Now,
                    UserGuid = new Guid(GetUserGuid()),
                    UserName = GetUserName()
                });

                ////SandboxContentProviderHost.CurrentProvider.UpdateChange(uid, entry);
                SandboxContentProviderHost.UpdateChange(uid, entry);
            }

            return new EmptyResult();
        }

        [HttpGet]
        [Authorize(Roles = "Developer")]
        public ActionResult Download()
        {
            IEnumerable<ChangeEntry> entries = SandboxContentProviderHost.GetChangeEntries().Where(x => x.Submitted);
            ////IEnumerable<ChangeEntry> entries = SandboxContentProviderHost.CurrentProvider.GetChanges().Where(x => x.Submitted);

            var archive = new ZipFile();
            var settings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

            foreach (var update in entries)
            {
                string content = JsonConvert.SerializeObject(update.ChangeLog, settings);

                switch (update.EntryType)
                {
                    case WeenieChange.TypeName:
                        Weenie w = update.ChangeLog as Weenie;
                        archive.AddFile($"weenies/{FormatFileName(w.WeenieClassId, w.Name)}", content);
                        break;

                    case SpawnMapChange.TypeName:
                        SpawnMapEntry s = update.ChangeLog as SpawnMapEntry;
                        archive.AddFile($"spawnMaps/{FormatFileName(s.Key, s.Description)}", content);
                        break;

                    case RecipeChange.TypeName:
                        Recipe r = update.ChangeLog as Recipe;
                        archive.AddFile($"recipes/{FormatFileName(r.Key, r.Description)}", content);
                        break;

                    case SpellTableChange.TypeName:
                        // ?
                        break;
                }
            }

            System.IO.Stream stream = archive.BuildZipStream();
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            return File(stream, "application/zip", $"LSD-Sandbox-{DateTime.Now:yyyy-MM-dd_hh-mm}.zip");
        }
    }
}