/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Lifestoned.Providers;

using Lifestoned.DataModel.Content;
using Lifestoned.DataModel.Shared;

namespace DerethForever.Web.Controllers
{
    public class PatchController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Patches()
        {
            return JsonGet(ContentProviderHost.PatchContentProvider.GetPatch());
        }

        [HttpGet]
        public ActionResult Content(int patchId)
        {
            return JsonGet(ContentProviderHost.PatchContentProvider.GetPatchContent(patchId));
        }

        [HttpPost]
        public ActionResult Save(Patch patch)
        {
            // can't trust the auth attribute here
            if (!User.Identity.IsAuthenticated || !User.IsInRole("Admin"))
                return UnauthorizedResult(true);

            ContentProviderHost.PatchContentProvider.SavePatch(patch);
            return OkResult();
        }

        [HttpPost]
        public ActionResult SaveContent(PatchContent content)
        {
            // can't trust the auth attribute here
            if (!User.Identity.IsAuthenticated || !User.IsInRole("Admin"))
                return UnauthorizedResult(true);

            ContentProviderHost.PatchContentProvider.SavePatchContent(content);
            return OkResult();
        }

        [HttpPost]
        public ActionResult DeleteContent(PatchContent content)
        {
            // can't trust the auth attribute here
            if (!User.Identity.IsAuthenticated || !User.IsInRole("Admin"))
                return UnauthorizedResult(true);

            ContentProviderHost.PatchContentProvider.DeletePatchContent(content.PatchId, content.ContentType, content.ContentId);
            return OkResult();
        }

        [HttpPost]
        public ActionResult CheckOutContent(PatchContent content)
        {
            // can't trust the auth attribute here
            if (!User.Identity.IsAuthenticated)
                return UnauthorizedResult(true);

            PatchContent pc = ContentProviderHost.PatchContentProvider.GetPatchContent(content.PatchId, content.ContentType, content.ContentId);
            if (pc == null)
                return HttpNotFound("Specified content not found! It may have been deleted.");

            pc.UserAssigned = new Guid(GetUserGuid());
            pc.LastModified = DateTime.Now;

            ContentProviderHost.PatchContentProvider.SavePatchContent(pc);

            return JsonGet(pc.UserAssigned);
        }

        [HttpPost]
        public ActionResult CheckInContent(PatchContent content)
        {
            // can't trust the auth attribute here
            if (!User.Identity.IsAuthenticated)
                return UnauthorizedResult(true);

            PatchContent pc = ContentProviderHost.PatchContentProvider.GetPatchContent(content.PatchId, content.ContentType, content.ContentId);
            if (pc == null)
                return HttpNotFound("Specified content not found! It may have been deleted.");

            pc.UserAssigned = Guid.Empty;
            pc.LastModified = DateTime.Now;

            ContentProviderHost.PatchContentProvider.SavePatchContent(pc);

            return OkResult();
        }
    }
}
