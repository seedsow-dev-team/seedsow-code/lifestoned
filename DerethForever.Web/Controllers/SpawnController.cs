﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.Providers;
using log4net;
using Newtonsoft.Json;

namespace DerethForever.Web.Controllers
{
    public class SpawnController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ISpawnMapProvider Provider => ContentProviderHost.GetProvider<ISpawnMapProvider>(SpawnMapChange.TypeName);

        private ISpawnMapSandboxProvider SandboxProvider => SandboxContentProviderHost.GetProvider<ISpawnMapSandboxProvider>(SpawnMapChange.TypeName);

        // GET: Spawn
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Map(uint id)
        {
            SpawnMapEntry result = null;
            if (User.Identity.IsAuthenticated)
            {
                SpawnMapChange change = SandboxProvider.GetChange(new Guid(GetUserGuid()), id) as SpawnMapChange;
                result = change?.Entry;
            }

            if (result == null)
                result = Provider.Get(id);

            return JsonGet(result);
            ////SpawnMapChange change = SandboxContentProviderHost.CurrentProvider.GetLandblockChange(id);
            ////if (change != null)
            ////    return JsonGet(change.Entry);
            ////return JsonGet(SandboxContentProviderHost.CurrentProvider.GetLandblock(id));
        }

        public ActionResult Search(string query)
        {
            if (User.Identity.IsAuthenticated)
            {
                return JsonGet(SandboxProvider.Search(new Guid(GetUserGuid()), s => s.Description == query));
            }
            else
            {
                return JsonGet(Provider.Search(s => s.Description == query));
            }
            ////return JsonGet(SandboxContentProviderHost.CurrentProvider.SearchLandblocks(query));
        }

        [Authorize]
        [HttpPost]
        public ActionResult New(SpawnMapEntry map)
        {
            map.LastModified = DateTime.Now;
            map.ModifiedBy = GetUserName();
            Guid uid = new Guid(GetUserGuid());

            if (SandboxProvider.GetChange(uid, map.Key) != null)
                return Json(null);

            SandboxProvider.UpdateChange(uid, map);
            return Json(map);

            ////if (SandboxContentProviderHost.CurrentProvider.GetLandblock(map.Key) != null)
            ////    return Json(null);

            ////SandboxContentProviderHost.CurrentProvider.SaveLandblock(map);
            ////return Json(map);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Save(SpawnMapEntry map)
        {
            map.LastModified = DateTime.Now;
            map.ModifiedBy = GetUserName();

            SandboxProvider.UpdateChange(new Guid(GetUserGuid()), map);
            ////SandboxContentProviderHost.CurrentProvider.SaveLandblock(map);
            return RedirectToAction("Index", "Sandbox");
        }

        [HttpGet]
        public ActionResult Download(uint id, string userGuid = null)
        {
            SpawnMapEntry map = null;

            if (!string.IsNullOrWhiteSpace(userGuid))
            {
                // id specified: is it us, or are we an admin
                if (string.Compare(userGuid, GetUserGuid(), true) == 0 || User.IsInRole("Developer"))
                {
                    SpawnMapChange change = SandboxProvider.GetChange(new Guid(GetUserGuid()), id) as SpawnMapChange;
                    ////SpawnMapChange change = SandboxContentProviderHost.CurrentProvider.GetLandblockChange(id, userGuid);
                    map = change.Entry;
                }
            }
            else
            {
                map = Provider.Get(id);
                ////map = ContentProviderHost.CurrentProvider.GetLandblock(id);
            }

            if (map == null)
                return new HttpNotFoundResult();

            JsonSerializerSettings s = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            string content = JsonConvert.SerializeObject(map, Formatting.None, s);
            string name = map.Description;
            foreach (char ifn in System.IO.Path.GetInvalidFileNameChars())
                name = name.Replace(ifn, '_');

            string filename = $"{name}.json";
            return File(Encoding.UTF8.GetBytes(content), "application/json", filename);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadItem()
        {
            string fileNameCopy = "n/a";

            try
            {
                Guid uid = new Guid(GetUserGuid());

                foreach (string fileName in Request.Files)
                {
                    fileNameCopy = fileName;
                    HttpPostedFileBase file = Request.Files[fileName];
                    uint id = 0;

                    using (MemoryStream memStream = new MemoryStream())
                    {
                        file.InputStream.CopyTo(memStream);
                        byte[] data = memStream.ToArray();

                        string serialized = Encoding.UTF8.GetString(data);
                        SpawnMapEntry item = JsonConvert.DeserializeObject<SpawnMapEntry>(serialized);
                        id = item.Key;

                        ////string token = GetUserToken();

                        item.LastModified = DateTime.Now;
                        item.ModifiedBy = GetUserName();

                        // save it
                        SandboxProvider.UpdateChange(uid, item);
                        ////SandboxContentProviderHost.CurrentProvider.SaveLandblock(item);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error parsing uploaded file {fileNameCopy}.", ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            return new EmptyResult();
        }

        [Authorize(Roles = "Developer")]
        [HttpGet]
        public ActionResult Compile()
        {
            SpawnMap map = new SpawnMap();

            ////foreach (SpawnMapEntry entry in ContentProviderHost.CurrentProvider.GetLandblocks())
            foreach (SpawnMapEntry entry in Provider.Get())
            {
                map.Entries.Add(entry);
            }

            string now = DateTime.UtcNow.ToString();
            map.Comment = $"Created by {GetUserName()} at {now}";

            JsonSerializerSettings s = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            string content = JsonConvert.SerializeObject(map, Formatting.None, s);

            string name = $"worldspawns-{now}";
            foreach (char ifn in System.IO.Path.GetInvalidFileNameChars())
                name = name.Replace(ifn, '_');

            string filename = $"{name}.json";
            return File(Encoding.UTF8.GetBytes(content), "application/json", filename);
        }
    }
}