﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Lifestoned.DataModel.Gdle.Spawns
{
    public class SpawnLink
    {
        [JsonProperty("target")]
        public int Target { get; set; }

        [JsonProperty("source")]
        public int Source { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }
    }
}
