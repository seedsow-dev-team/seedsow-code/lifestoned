﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lifestoned.DataModel.Content;
using Lifestoned.DataModel.Shared;

namespace Lifestoned.Providers
{
    public interface IPatchContentProvider
    {
        IEnumerable<Patch> GetPatch();
        Patch GetPatch(int id);
        bool SavePatch(Patch item);
        bool UpdatePatch(Patch item);
        bool DeletePatch(int id);

        IEnumerable<PatchContent> GetPatchContent(int patchId);
        IEnumerable<PatchContent> GetPatchContent(int patchId, ContentType type);
        PatchContent GetPatchContent(int patchId, ContentType type, int id);
        bool SavePatchContent(PatchContent item);
        bool UpdatePatchContent(PatchContent item);
        bool DeletePatchContent(int patchId, ContentType type, int id);
    }
}
