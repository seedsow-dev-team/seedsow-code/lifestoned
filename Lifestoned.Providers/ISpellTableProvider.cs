﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lifestoned.DataModel.Gdle.Spells;

namespace Lifestoned.Providers
{
    public interface ISpellTableProvider : IGenericContentProvider<SpellTableEntry>
    {
    }

    public interface ISpellTableSandboxProvider : ISpellTableProvider, IGenericSandboxContentProvider<SpellTableEntry>
    {
    }
}
