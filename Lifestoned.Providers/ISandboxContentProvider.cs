/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Shared;

namespace Lifestoned.Providers
{
	public interface IGenericSandboxContentProvider : IGenericContentProvider
    {
        IGenericContentProvider ContentProvider { get; set; }

        ChangeEntry GetChangeEntry(Guid userId, uint id);

        IEnumerable<ChangeEntry> GetChangeEntries();
        IEnumerable<ChangeEntry> GetChangeEntries(Guid userId);

        void UpdateChange(Guid userId, ChangeEntry entry);
        void AcceptChange(Guid userId, ChangeEntry entry);
        void DeleteChange(Guid userId, ChangeEntry entry);
    }

    public interface IGenericSandboxContentProvider<T> : IGenericSandboxContentProvider, IGenericContentProvider<T> where T : class, IMetadata
    {
		IGenericContentProvider<T> ContentProviderT { get; }

        ChangeEntry<T> GetChange(Guid userId, uint id);

        IEnumerable<ChangeEntry<T>> GetChanges();
        IEnumerable<ChangeEntry<T>> GetChanges(Guid userId);

        void UpdateChange(Guid userId, T entry);
        void AcceptChange(Guid userId, T entry);
        void DeleteChange(Guid userId, T entry);

        IQueryable<T> Search(Guid userId, Expression<Func<T, bool>> criteria);
    }

    public interface ISandboxContentProvider : IContentProvider//, ISpawnMapSandboxProvider
    {
        IEnumerable<ChangeEntry> GetChanges();

        IEnumerable<ChangeEntry> GetChanges(string type);

        IEnumerable<ChangeEntry> GetChanges(Guid userId);

        IEnumerable<ChangeEntry> GetChanges(Guid userId, string type);

        void UpdateChange(Guid userId, ChangeEntry change);

        void AcceptChange(Guid userId, ChangeEntry change);

        void DeleteChange(Guid userId, ChangeEntry change);

        List<WeenieChange> GetAllWeenieChanges();

        List<WeenieChange> GetMyWeenieChanges(string token);

        void UpdateWeenieChange(string changeOwnerGuid, WeenieChange wc);

        void DeleteWeenieChange(string changeOwnerGuid, WeenieChange wc);

        Weenie GetWeenieFromSource(string token, uint weenieClassId);

        WeenieChange GetSandboxedChange(Guid userGuid, uint weenieClassId);

        WeenieChange GetMySandboxedChange(string token, uint weenieClassId);

        bool UpdateWeenieInSource(string token, Weenie weenie);

        void ReloadCache();

        SpawnMapChange GetLandblockChange(uint id);
        SpawnMapChange GetLandblockChange(uint id, string userId);
    }
}
