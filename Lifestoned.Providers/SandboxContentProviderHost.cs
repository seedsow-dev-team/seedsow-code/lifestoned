/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.Providers
{
    public class SandboxContentProviderHost
    {
        private static Dictionary<string, IGenericSandboxContentProvider> providers = new Dictionary<string, IGenericSandboxContentProvider>();

		public static void AddProvider(string key, IGenericSandboxContentProvider provider)
        {
			if (provider.ContentProvider == null)
                provider.ContentProvider = ContentProviderHost.GetProvider(key);

            providers.Add(key, provider);
        }

		public static IGenericSandboxContentProvider GetProvider(string key)
        {
            providers.TryGetValue(key, out IGenericSandboxContentProvider result);
            return result;
        }

        public static T GetProvider<T>(string key) where T : class, IGenericSandboxContentProvider
        {
            providers.TryGetValue(key, out IGenericSandboxContentProvider result);
            return result as T;
        }

        public static ChangeEntry GetChangeEntry(string type, Guid userId, uint id)
        {
            return GetProvider(type)?.GetChangeEntry(userId, id);
        }

        public static IEnumerable<ChangeEntry> GetChangeEntries()
        {
            return providers.Values.Select(p => p.GetChangeEntries()).SelectMany(ce => ce);
        }

        public static IEnumerable<ChangeEntry> GetChangeEntries(Guid userId)
        {
            return providers.Values.Select(p => p.GetChangeEntries(userId)).SelectMany(ce => ce);
        }

        public static IEnumerable<ChangeEntry> GetChangeEntries(string type)
        {
            return GetProvider(type)?.GetChangeEntries();
        }

        public static IEnumerable<ChangeEntry> GetChangeEntries(string type, Guid userId)
        {
            return GetProvider(type)?.GetChangeEntries(userId);
        }

        public static void UpdateChange(Guid userId, ChangeEntry entry)
        {
            GetProvider(entry.EntryType)?.UpdateChange(userId, entry);
        }

        public static void AcceptChange(Guid userId, ChangeEntry entry)
        {
            GetProvider(entry.EntryType)?.AcceptChange(userId, entry);
        }

        public static void DeleteChange(Guid userId, ChangeEntry entry)
        {
            GetProvider(entry.EntryType)?.DeleteChange(userId, entry);
        }

        public static ISandboxContentProvider CurrentProvider { get; set; }

		public static IRecipeSandboxProvider CurrentRecipeProvider { get; set; }

        public static ISpellTableSandboxProvider CurrentSpellTableProvider { get; set; }
    }
}
