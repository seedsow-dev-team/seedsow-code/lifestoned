﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle;

namespace Lifestoned.Providers.Database
{
    public class DatabaseWeenieProvider : SQLiteContentDatabase<Weenie>, IWeenieProvider
    {
		public DatabaseWeenieProvider() : base("WeenieDbConnection", "Weenies", (o) => o.WeenieClassId)
        {
        }
    }

    public class DatabaseWeenieSandboxProvider : SQLiteSandboxDatabase<Weenie, WeenieChange>, IWeenieSandboxProvider
    {
        public DatabaseWeenieSandboxProvider() : base("WeenieDbConnection", "Weenies_Sandbox", (o) => o.WeenieClassId)
        {
        }
    }
}
